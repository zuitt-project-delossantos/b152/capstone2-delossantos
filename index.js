const express = require("express");
const mongoose  = require("mongoose");
const app = express();
const cors = require ("cors");
const port = process.env.PORT || 4000;

mongoose.connect("mongodb+srv://admin:admin123@cluster0.s1riy.mongodb.net/myFirstDatabase?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	})


let db = mongoose.connection;
db.on("error",console.error.bind(console, "Connection Error"));
db.once("open",()=>console.log("Connected to MongoDB"));

app.use(express.json());
app.use(cors())

const userRoutes = require('./routes/userRoutes');
app.use('/users',userRoutes);


const courseRoutes = require('./routes/courseRoutes');
app.use('/products',courseRoutes);



const orderRoutes = require('./routes/orderRoutes');
app.use('/orders',orderRoutes);




// cors is used to limit access to your application. with this we can allow/disallow certain applications from accessing our app.
app.use(cors());





app.listen(port,()=>console.log(`Server running at localhost:4000`));
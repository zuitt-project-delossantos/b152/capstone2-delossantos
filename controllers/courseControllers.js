const Product = require("../models/Product");





module.exports.addProduct = (req,res) => {

	console.log(req.body);


	let newProduct = new Product({

		name: req.body.name,
		description: req.body.description,
		price: req.body.price

	})

	newProduct.save()
	.then(product => res.send(product))
	.catch(error => res.send(error))


}




module.exports.getAllProducts = (req,res) => {

	
	Product.find({isActive:true})
	.then(result => res.send(result))
	.catch(error => res.send(error));

}





module.exports.getSingleProduct = (req,res) => {

	

	Product.findById(req.params.id)
	.then(result => res.send(result))
	.catch(err => res.send(err))

}






module.exports.updateProduct = (req,res) => {


	let updates = {

		name: req.body.name,
		description: req.body.description,
		price: req.body.price

	}

	
	Product.findByIdAndUpdate(req.params.id,updates,{new:true})
	.then(updatedProduct => res.send(updatedProduct))
	.catch(err => res.send(err));

}






module.exports.archive = (req,res) => {

	
	let updates = {
		isActive: false
	}

	Product.findByIdAndUpdate(req.params.id,updates,{new:true})
	.then(updatedProduct => res.send(updatedProduct))
	.catch(err => res.send(err));

}
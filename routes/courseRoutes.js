const express = require("express");
const router = express.Router();
const courseControllers = require("../controllers/courseControllers");

const auth = require("../auth");
const {verify,verifyAdmin} = auth;



router.post('/',verify,verifyAdmin,courseControllers.addProduct);
router.get('/',courseControllers.getAllProducts);
router.get('/getSingleProduct/:id',courseControllers.getSingleProduct);



router.put('/:id',verify,verifyAdmin,courseControllers.updateProduct);
router.put('/archive/:id',verify,verifyAdmin,courseControllers.archive);


module.exports = router;
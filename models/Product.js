const mongoose = require("mongoose");







let productSchema = new mongoose.Schema({

	name: {
		type: String,
		required: [true, "Name is required."]
	},
	description: {
		type: String,
		required: [true, "Description Name is required."]
	},
	price: {
		type: Number,
		required: [true, "Price is required."]
	},
	isActive: {
		type: Boolean,
		default: true
	},
	CreatedOn: {
		type: Date,
		default: [new Date]
	},
	orders: [

		{
			orderId: {
				type: String,
				required: true
			},
			quantity: {
				type: Number,
				required: true
			}
		}

	]
	

})

module.exports = mongoose.model("Product",productSchema);

const mongoose = require("mongoose");



const orderSchema = new mongoose.Schema({

	userId: {
		type: String,
		required: [true, "ID is required."]
	},
	totalAmount: {
		type: Number,
		required: [true, "Number is required."]
	},
	purchasedOn: {
		type: Date,
		default: new Date()
	},
	products: [

		{

			productId:  {
				type: String,
				required: [true, "ID is required."]
			},
			quantity:  {
				type: Number,
				required: [true, "Quantity required"]
			}
		}
	]
})
module.exports = mongoose.model("Order",orderSchema);